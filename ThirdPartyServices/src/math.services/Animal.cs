﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace math.services
{
    public class Animal
    {
        public virtual void sleep()
        {
            Console.WriteLine("Animals are sleeping");
        }

        public virtual void eat()
        {
            Console.WriteLine("Animals are eating");
        }
    }

    public class Cat : Animal    // inheritance
    {
        public override void sleep()
        {
            Console.WriteLine("Cat is sleeping");
        }

        public override sealed void eat()  // sealed method
        {
            Console.WriteLine("Cat is eating");
        }
    }

    public class Dog : Cat  // inheritance
    {
    }

    public class TestSealed
    {
        public static void Main()    // main method
        {
            Dog d = new Dog();
            d.eat();
        }
    }
}