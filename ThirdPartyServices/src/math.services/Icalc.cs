﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace math.services
{
    public interface Icalc<T>
    {
        T Add(T value1, T value2);

        T Subtract(T value1, T value2);
    }
}